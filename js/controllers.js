angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

})

.controller('PlaylistCtrl', function($scope, service,$state, $ionicSlideBoxDelegate, $ionicGesture,$localstorage,$translate,$ionicPopup) {
$scope.user={};
  var lang = $localstorage.get('lang');

    if(typeof(lang) != 'undefined'){
        $translate.use(lang);
    }

    var obj = {};
    obj.token = 9791047476;
    rasis = [];
    $scope.match_result = {};
    bride_name = '';
    groom_name = '';
  // $scope.match_result.girlraasi = "hai";
   service.post(url + 'getRaasis', obj).then(function(result) {
    // console.log(result);
    rasis = result;
          $scope.rasi = result;
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          $scope.news_nodata = true;
          $scope.news_loading_icon = false;
          $scope.news_loadmore = false;
          // $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });

   $scope.getAllopt = function (){
       var obj = {'token':token};
       service.post(url + 'getAlloptions', obj).then(function(result) {
console.log(result);
       $scope.girlopt = result.girlstar;
       $scope.boyopt = result.boystar;
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          $scope.news_nodata = true;
          $scope.news_loading_icon = false;
          $scope.news_loadmore = false;
          // $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });

   }

   $scope.getAllopt();


$scope.selectstar= function(id, type)
{

 var obj1 = {};
  obj1.token = 9791047476;
obj1.raasi_id=id;

 service.post(url + 'getStars', obj1).then(function(result) {
  console.log(result);
    if(type=='bride')
    {
        $scope.bride_stars = result;
    }
    else if(type=='groom')
    {
       $scope.groom_stars = result;
    }
console.log(rasis);

    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.news_nodata = true;
        $scope.news_loading_icon = false;
        $scope.news_loadmore = false;
      }
    });
}

$scope.match = function(user)
{

console.log(JSON.stringify(user));


  user.token = token;
  service.post(url + 'getkalyanaporuttham', user).then(function(result) {
console.log(result);
    if (result.status == 'success') {
      if(result.match.k == 1){
delete result.match.k;
if(result.match.k == "" || result.match.k == null){
  result.match.count -= 1;
    $localstorage.setObject('matresult',result.match);
        $state.go('app.result');
      }
}
else{
  delete result.match.k;
  if(result.match.k == "" || result.match.k == null){
      $localstorage.setObject('matresult',result.match);
          $state.go('app.result');
        }
}
    }
    else{
      $ionicPopup.show({
  template: result.message,
  title: 'Validation Failed',
  scope: $scope,
  buttons: [
            {    text: '<b>OK</b>',
               type: 'button-assertive  button-small col-50 col-offset-50',  onTap: function(e) {
                console.log(e);
                return true;
              }
         }
           ]
});
    }

  })



}

   $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
})


.controller('ResultCtrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory, $localstorage,$ionicPopup) {


    $scope.ratingsObject = {
        iconOn: 'ion-ios-star',    //Optional
        iconOff: 'ion-ios-star-outline',   //Optional
        iconOnColor: 'rgb(255, 149, 38)',  //Optional
        iconOffColor:  'rgb(255, 149, 38)',    //Optional
        rating:  2, //Optional
        minRating:1,    //Optional
        readOnly: true, //Optional
        callback: function(rating, index) {    //Mandatory
          $scope.ratingsCallback(rating, index);
        }
      };

      $scope.ratingsCallback = function(rating, index) {
        console.log('Selected rating is : ', rating, ' and the index is : ', index);
        $localstorage.set('rating',rating);

        console.log($localstorage.get('rating'))

      };

   $scope.groups_tamil = [{
      title: "தின பொருத்தம்",
      num:1,
      params:'a',
      contents: [
        {
          line: "இதை நட்சத்திரப் பொருத்தம் என்று சொல்லலாம். கணவன், மனைவி இருவரும் நோய், நொடி இன்றி ஆரோக்கியத்துடனும், சுகத்துடனும் இருக்க​ தினப் பொருத்தம் அவசியம். இந்த​ பொருத்தம் இவருடைய​ எண்ணங்களும் ஒன்றாக​ ஒத்திருப்பதைக் காட்டும்."
        }
      ]
    },
    {
      title: " கண பொருத்தம்",
      num:0,
      params:'b',
      contents: [
        {
          line: "இருவரின் குண​ வேறுபாடுகள், காரிய​ சாதனைகள் மற்றும் தாம்பத்திய​ வாழ்க்கையில் சுகபோக​ பாக்கியங்களை குறைவின்றி அனுபவிக்க​ கணப் பொருத்தம் அவசியமாகிறது."
        }
      ]
    },
    {
      title: "மகேந்திர பொருத்தம்",
      num:0,
      params:'c',
      contents: [
        {
          line: "குலம் விருத்தியடைய​ புத்திர​ பாக்கியம் இருக்க​ வேண்டும். மஹேந்திர​ பொருத்தம் இருந்தால் புத்திர​ பாக்கியம் விருத்தியடையும்."
        }
      ]
    },
    {
      title: "ஸ்ரீ தீர்க்க பொருத்தம்",
      num:1,
      params:'d',
      contents: [
        {
          line: "அஷ்டலட்சுமி கடாட்சமும், தனதான்ய​ விருத்தியும் தருவது ஸ்திரீ தீர்க்கப் பொருத்தமாகும். ஸ்திரீக்கு குறைவேதும் ஏற்படாமல் இருக்க​ இந்தப்பொருத்தம் அவசியமானதாகும்."
        }
      ]
    },
      {
      title: "யோனி பொருத்தம்",
      num:1,
      params:'e',
      contents: [
        {
          line: "கணவன், மனைவி இருவரும் பரஸ்பர​ அன்புடனும், பாசத்துடனும் குடும்பம் நடத்தவும், ஒருவர் பாதையை மற்றவர் புரிந்து நடக்கவும், நல்ல​ தாம்பத்திய​ உறவு மேம்படவும் யோனிப்பொருத்தம் இருக்க​ வேண்டும்."
        }
      ]
    },
    {
      title: "ராசி பொருத்தம்",
      num:0,
      params:'f',
      contents: [
        {
          line: "கணவன், மனைவி இருவருக்குமிடையே நல்ல​ ஒற்றுமையும், குடும்ப​ சுகமும், புத்திர​ பாக்கியமும், உறவினர்களின் ஒற்றுமையும் அமைய​ ராசி பொருத்தம் முக்கியமானதாகும்."
        }
      ]
    },
    {
      title: "ராசி அதிபதி பொருத்தம்",
      num:1,
      params:'g',
      contents: [
        {
          line: "கணவன், மனைவி இருவருக்குமிடையே உள்ள​ ஒற்றுமையை குறிக்கும். இருவரின் கருத்துகளும் ஒன்றுபட்டுள்ளதா அல்லது மாறுபட்டு உள்ளதா என்பதை ராசியதிபதியை கொன்டு நிர்ணயிக்கலாம்."
        }
      ]
    },
    {
      title: "வசிய பொருத்தம்",
      num:1,
      params:'h',
      contents: [
        {
          line: "கணவன், மனைவி இருவரும் பரஸ்பர​ அன்புடனும், காதலுடனும் வசியப்பட்டு வாழ​ வேண்டுமானால் வசியப்பொருத்தம் அவசியம் வெண்டும்."
        }
      ]
    },
    {
      title: "ரஜ்ஜு பொருத்தம்",
      num:1,
      params:'i',
      contents: [
        {
          line: "கணவன், மனைவி இருவரும் தீர்க்காயுளுடன் இருக்க​ வேண்டும். மனைவி தீர்க்க​ சுமங்கலியாக​ இருக்க​ வேண்டும். பெண்ணின் மாங்கல்ய​ பலத்தை குறிப்பதால் இந்த​ பொருத்தம் மிகவும் முக்கியமானதாகும். இதைக்கழுத்து பொருத்தம் என்றும், தாலி பொருத்தம் என்றும் அழைக்கின்றார்கள்."
        }
      ]
    },
    {
      title: "வேதை பொருத்தம்",
      num:1,
      params:'j',
      contents: [
        {
          line: "வேதை என்றால் இடையூறு என்று பொருள். வாழ்வின் உயர்விலும், தாழ்விலும் கோப​, தாபங்கள், கஷ்ட​, நஷ்டங்கள் போன்ற​ காலங்களில் மனோ பலத்துடன் அவற்றை இருவரும் ஒற்றுமையாய் தாங்கிக்கொள்ளும் வகையில் வேதைப் பொருத்தம் அமைவதால் இது மிகவும் முக்கியமான​ பொருத்தமாகும்."
        }
      ]
    }
  ];

   $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };

    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };



 var res =  $stateParams.obj;


 if(res){
    $scope.match_result = $stateParams.obj;
    $localstorage.setObject('result',$stateParams.obj);

    var couple = {};
    couple.bride_name = $stateParams.bride_name;
    couple.groom_name = $stateParams.groom_name;

    $scope.bride_name = $stateParams.bride_name;
    $scope.groom_name = $stateParams.groom_name;

    $localstorage.setObject('couple',couple);

 }else{
    var temp_res = $localstorage.getObject('result');
    var couple = $localstorage.getObject('couple');

    $scope.match_result = temp_res;

    $scope.bride_name = couple.bride_name;
    $scope.groom_name = couple.groom_name;

 }

$scope.groups = [{
      title: "Dina Porutham",
      // num:1,
      params:'a',
      contents: [
        {
          line: "Dina means daily and this evaluates whether the boy and girl can get along on a day to day basis. Also called Tara Porutham it uses the Muhurtha technique used to fix events on a favourable nakshatra day, to find a compatible person."
        }
      ]
    },
    {
      title: "Gana Porutham",
      // num:0,
      params:'b',
      contents: [
        {
          line: "A samskrita word for class, group, tribe; the nakshatras are grouped into 3 - Deva, Manushya & Rakshasa. A very simple way to show the ideal human being, the average human personality and a less human person. Similar groups or closer groups will get along better…right!"
        }
      ]
    },
    {
      title: "Mahendra Porutham",
      // num:0,
      params:'c',
      contents: [
        {
          line: "Placement of the boy’s nakshatra at certain distance from the girl’s is presumed to ensure children in large number. The era of birth control and small families, has maybe diminished the importance of this porutham."
        }
      ]
    },
    {
      title: "Stree Deergha Porutham",
      // num:1,
      params:'d',
      contents: [
        {
          line: "Placement of the boy’s nakshatra beyond a certain distance from the girl’s is presumed to ensure a good quality married life for the girl. Also known as Sumangali Porutham, it indicates a long, prosperous and happy life for the girl."
        }
      ]
    },
      {
      title: "Yoni Porutham",
      // num:1,
      params:'e',
      contents: [
        {
          line: "Sexual tendencies are fixed by classifying the nakshatras into 14 groups and then ranking which gets along Best, Ok, Worst. Interestingly, animal species are used to name these categories!"
        }
      ]
    },
    {
      title: "Rashi Porutham",
      // num:0,
      params:'f',
      contents: [
        {
          line: "The mutual disposition of the two Janma Rashis is examined to ascertain this. A favourable match is presumed to ensure family harmony (like getting along with in-laws), prosperity and birth of children."
        }
      ]
    },
    {
      title: "Rasaydhipa Porutham",
      // num:1,
      params:'g',
      contents: [
        {
          line: "This porutham is important for the wealth of their offspring."
        }
      ]
    },
    {
      title: "Vashya Porutham",
      // num:1,
      params:'h',
      contents: [
        {
          line: "Mutual affection and attraction is the focus of this factor. Boys born in certain Moon signs (Janma Rashi) will vibe well with certain Janma Rashi of girls."
        }
      ]
    },
    {
      title: "Rajju Porutham",
      // num:1,
      params:'i',
      contents: [
        {
          line: "The nakshatras are split into 5 groups. Each group represents one part of the body. It is considered inauspicious to have both Janma Nakshatras in the same group. This has no points in the overall score, but a simple YES/No."
        }
      ]
    },
    {
      title: "Vedha Porutham",
      // num:1,
      params:'j',
      contents: [
        {
          line: "Every nakshatra does not get along with one other nakshatra. This is similar to Rajju and has a YES/No as an answer."
        }
      ]
    }
  ];

angular.forEach($scope.groups,function(value,key){
    if(value.params == 'a')
    {
    value.num = $scope.match_result.a;
    }
    else if(value.params == 'b')
    {
    value.num = $scope.match_result.b;
    }
    else if(value.params == 'c')
    {
    value.num = $scope.match_result.c;
    }
    else if(value.params == 'd')
    {
    value.num = $scope.match_result.d;
    }
    else if(value.params == 'e')
    {
    value.num = $scope.match_result.e;
    }
    else if(value.params == 'f')
    {
    value.num = $scope.match_result.f;
    }
    else if(value.params == 'g')
    {
    value.num = $scope.match_result.g;
    }
    else if(value.params == 'h')
    {
    value.num = $scope.match_result.h;
    }
    else if(value.params == 'i')
    {
    value.num = $scope.match_result.i;
    }
    else if(value.params == 'j')
    {
    value.num = $scope.match_result.j;
    }
})
angular.forEach($scope.groups_tamil,function(value,key){
    if(value.params == 'a')
    {
    value.num = $scope.match_result.a;
    }
    else if(value.params == 'b')
    {
    value.num = $scope.match_result.b;
    }
    else if(value.params == 'c')
    {
    value.num = $scope.match_result.c;
    }
    else if(value.params == 'd')
    {
    value.num = $scope.match_result.d;
    }
    else if(value.params == 'e')
    {
    value.num = $scope.match_result.e;
    }
    else if(value.params == 'f')
    {
    value.num = $scope.match_result.f;
    }
    else if(value.params == 'g')
    {
    value.num = $scope.match_result.g;
    }
    else if(value.params == 'h')
    {
    value.num = $scope.match_result.h;
    }
    else if(value.params == 'i')
    {
    value.num = $scope.match_result.i;
    }
    else if(value.params == 'j')
    {
    value.num = $scope.match_result.j;
    }
})
  console.log($scope.match_result);


  $scope.match_sub_count = $scope.match_result.count;


  $scope.backview = function ()
  {
    $backView = $ionicHistory.backView();
           $backView.go();
  }

   $scope.successmsg = function (msg,source) {
    //console.log(user)
    $ionicPopup.show({
      template: "<style>.popup { width:500px; }</style><p>"+msg+"<p/>",
      title: 'Message',
      //subTitle: 'MySubTitle',
      scope: $scope,
      buttons: [
       {
         text: '<b>OK</b>',
         type: 'button-positive  button-small col-50 col-offset-50',
         onTap: function() {
          if(source =='email'){
            $scope.gomail();
          }
          }
       }
      ]
    });
   }
  $scope.showPopup = function() {
  var popup = $ionicPopup.show({
    title: 'Enter Wi-Fi Password',
    subTitle: 'Please use normal things',
    scope: $scope,
    buttons: [
              { text: 'ready',  onTap: function(e) {
                  console.log(e);
                  return true;
                }
           }
             ]
  }).then(function(result){
    console.log('Tapped', result);
  }, function(error){
    console.log('error', error);
  }, function(popup){
    popup.close();
  })
 };


  $scope.gomail=function()
  {
    $scope.user = {};
    var mypopup = $ionicPopup.show({
    templateUrl: 'templates/email_pop_up.html',
    title: 'Enter Your Email Address',
    subTitle: '',
    scope: $scope,
    buttons: [
     { text: 'Cancel',
     onTap : function (){
      return 'cancel';
     }
      },
     {
       text: '<b>Send</b>',
       type: 'button-positive',
       onTap: function(e) {
        if(!angular.equals({}, $scope.user))
        {
            return $scope.user;
        }
        }
     }
    ]
  })
    mypopup.then(function(res) {
    console.log('Tapped!', res);

    if(res == 'cancel'){
      console.log('cancel')
      mypopup.close();
    }else{
      if(res != null)
    {

      var obj = $scope.match_result;
      obj.email = res.email;
      obj.name = res.name;
      obj.bride_name = $scope.bride_name;
      obj.groom_name = $scope.groom_name;
      obj.star = $localstorage.get('rating');
      //console.log($localstorage.get('rating'));
      if(obj.email!=null && obj.name!=null && obj.name!=null   )
      {

        service.post(url + 'sentPorutthamemail', obj).then(function(result) {
            $localstorage.set('ratings',null);


            $scope.successmsg('Mail sent successfully','');
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {

             $scope.successmsg('Network Failed','email');

          }
        });

      }
      else{

         $scope.successmsg('Name, Email & Ratings is Mandatory','email');


      }
    }
      else{
         $scope.successmsg('Name, Email & Ratings is Mandatory','email');

      }
    }
   /* */


  });

$scope.popupclose = function () {
   mypopup.close();
}

  }



var lang = $localstorage.get('lang');

if(lang == 'tamil'){
 $scope.explaining = $scope.groups_tamil;
}else{
 $scope.explaining = $scope.groups;
}
})



.controller('mainhomectrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory,$translate,$localstorage) {
    $scope.gotamil=function(lang)
    {
      $translate.use(lang);
      $localstorage.set('lang',lang);
      $state.go("app.playlist");
    }

})

.controller('matchtamilctrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory) {


})

.controller('matresultctrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory,$localstorage) {

if($localstorage.getObject('matresult') != "" || $localstorage.getObject('matresult') != null){

  $scope.match_result =   $localstorage.getObject('matresult');
  $scope.match_sub_count =   $scope.match_result.count;
  $scope.match_result.count=  $scope.match_sub_count;
    $scope.match_count = (  $scope.match_sub_count/10)*100;
}


  $scope.see_description = function()
  {

    $state.go('app.match_result', {obj:$scope.match_result});


  }

})

.controller('descriptionctrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory) {


})

.controller('mailpagectrl', function($scope,$state, $ionicSlideBoxDelegate, service, $stateParams, $ionicHistory) {

  $scope.getresult=$stateParams.obj;
 console.log($scope.getresult);

  $scope.domail=function(user)
  {

      console.log(user);
      var mail_data={};
      mail_data.token =9791047476;
      mail_data.name =user.name;
      mail_data.email =user.email;
      console.log(mail_data);

  }




})

.controller('ContactCtrl', function($scope,  $ionicPopup,service){
$scope.contact ={}


$scope.contact_form_Submit = function(contact){
console.log(contact);
var contactobj = {};

contactobj.token = token;
contactobj.name = contact.name;
contactobj.email = contact.email;
contactobj.message = contact.message;
console.log(contactobj);
service.post(url + 'contact_us', contactobj).then(function(result) {

  if (result.status == 'success') {

      $ionicPopup.show({
    template: result.message,
    title:'Success',
    scope: $scope,
    buttons: [
            {    text: '<b>OK</b>',
               type: 'button-balanced  button-small col-50 col-offset-50',  onTap: function(e) {
                console.log(e);
                return true;
              }
         }
           ]
    });

$scope.contact.email = "";
$scope.contact.name = "";
  $scope.contact.message = "";


  }else{
    $ionicPopup.show({
template: 'Validation Failed',
title: 'Error',
scope: $scope,
buttons: [
          {    text: '<b>OK</b>',
             type: 'button-assertive  button-small col-50 col-offset-50',  onTap: function(e) {
              console.log(e);
              return true;
            }
       }
         ]
});
  }

})
}

})
.controller('SettingsCtrl', function($scope,$cordovaInAppBrowser,$cordovaAppVersion){


        var options = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'no',
        closebuttoncaption: 'Back',
        transitionstyle: 'crossdissolve'
      };
   $scope.openBrowser = function(link) {

      $cordovaInAppBrowser.open(link, '_blank', options)
        .then(function(event) {
          console.log("success");
        })
        .catch(function(event) {
          console.log("failure");
        });

  }
    document.addEventListener("deviceready", function () {
  $cordovaAppVersion.getVersionNumber().then(function (version) {
     $scope.appVersion = version;
   });
 }, false);

});
