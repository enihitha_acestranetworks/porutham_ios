
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngCordova', 'starter.services', 'ionMdInput',
  'percentCircle-directive','pascalprecht.translate', 'ionic-ratings'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

	  	   if(window.plugins && window.plugins.AdMob) {
                var admob_key = device.platform == "Android" ? "ca-app-pub-6962332027734181/7587927082" : "ca-app-pub-6962332027734181/9648072076";
                var admob = window.plugins.AdMob;
                admob.createBannerView(
                    {
                        'publisherId': admob_key,
                        'adSize': admob.AD_SIZE.BANNER,
                        'bannerAtTop': false
                    },
                    function() {
                        admob.requestAd(
                            { 'isTesting': false },
                            function() {
                                admob.showAd(true);
                            },
                            function() { console.log('failed to request ad'); }
                        );
                    },
                    function() { console.log('failed to create banner view'); }
                );
            }



document.addEventListener("deviceready", function onDeviceReady(w) {
    // Should work on Andriod
    if(StatusBar && statusbarTransparent) {
        // Enable translucent statusbar
        statusbarTransparent.enable();

        // Get the bar back
        StatusBar.show();
    }
    // iOS only
    else if (StatusBar) {
        // Get the bar back
        StatusBar.show();
    }
}, false);


  });
})

.config(function($stateProvider, $urlRouterProvider,$translateProvider,$cordovaInAppBrowserProvider) {

  $translateProvider.translations('english', {
    HEADLINE: 'Marriage Compatibility',
    women_name: 'Bridal Name',
    women_rasi:'Bridal Raasi',
    women_star:'Bridal Star',
    men_name: 'Groom Name',
    men_rasi:'Groom Raasi',
    men_star:'Groom Star',
    mobile:'Mobile Number',
    see_match:'Match',
    result:'Result',
    total:'Total Count',
    explain:'Explanation',
    comment:'Comment',
    count:'Match Count',
    mail:'Send Email',
    email:'Email',
    name:'Name',
    send:'Send',
    Rating:'Rating',
    Bride:'Bride',
    Groom:'Groom'

  });

  $translateProvider.translations('tamil', {
    HEADLINE: 'திருமணப் பொருத்தம்',
    women_name: 'மணமகள் பெயர்',
    women_rasi:'மணமகள் ராசி',
    women_star:'மணமகள் நட்சத்திரம்',
    men_name: 'மணமகன் பெயர்',
    men_rasi:'மணமகன் ராசி',
    men_star:'மணமகன் நட்சத்திரம்',
    mobile:'மொபைல் எண்',
    see_match:'பொருத்தம் பார்க்க',
    result:'முடிவுகள்',
    total:'மொத்தம்',
    explain:'விளக்கம்',
    comment:'கருத்து',
    count:'பொருத்தம் எண்',
    mail:'மின்னஞ்சல் செய்க',
    email:'மின்னஞ்சல்',
    name:'பெயர்',
    send:'அனுப்புக',
    Rating:'மதிப்பீடு',
    Bride:'மணமகள்',
    Groom:'மணமகன்'
  });

  var defaultOptions = {
    location: 'no',
    clearcache: 'no',
    toolbar: 'no'
  };

    $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions);


  $translateProvider.preferredLanguage('english');



  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    cache:true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller:'ContactCtrl'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller:'SettingsCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.playlist', {
    url: '/playlist',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

  .state('app.mainhome', {
    url: '/mainhome',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/mainhome.html',
        controller: 'mainhomectrl'
      }
    }
  })

  .state('app.match', {
    url: '/matchtamil',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/match.html',
        controller: 'matchtamilctrl'
      }
    }
  })

  .state('app.result', {
    url: '/result',
    cache:false,
    params:{obj:null},
    views: {
      'menuContent': {
        templateUrl: 'templates/result.html',
        controller: 'matresultctrl'
      }
    }
  })

  .state('app.description', {
    url: '/description',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/description.html',
        controller: 'descriptionctrl'
      }
    }
  })

  .state('app.mailpage', {
    url: '/mailpage',
    cache:false,
    params:{obj:null,bride_name:null,groom_name:null},
    views: {
      'menuContent': {
        templateUrl: 'templates/mailpage.html',
        controller: 'mailpagectrl'
      }
    }
  })

  .state('app.match_result', {
    url: '/match_result',
    cache:false,
    params:{obj:null},
    views: {
      'menuContent': {
        templateUrl: 'templates/match_result.html',
        controller: 'ResultCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/app/mainhome');
})
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);

var url='https://www.kalyanseva.com/api/';
/*var url='http://kalyanseva/api/';*/

var token = 9791047476;
